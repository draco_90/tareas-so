# Tareas - Sistemas Operativos

Semestre 2021-2

En este repositorio estaremos manejando las tareas de la materia de [sistemas operativos][url-sistemas_operativos-fciencias] que se imparte en la Facultad de Ciencias, UNAM en el semestre 2021-2.

## Entregas

- Las entregas de algunas tareas y todas las prácticas se realizan **en equipo** de 
3 o 4 personas
- **No se aceptan trabajos individuales** para las entregas diseñadas para realizarse en equipo como se explica en 
[la presentacion del curso](http://www.fciencias.unam.mx/docencia/horarios/presentacion/322385)

## Estructura de directorios

La estructura de directorios para la entrega de tareas o practicas será la siguiente:
- Una carpeta para cada tarea (ej. `tarea-01`) o práctica (ej. `practica-01`)
- Cada carpeta tendrá un archivo `README.md` donde se especificará que se debe 
  de entregar
- Para las entregas individuales cada alumno creará una carpeta nueva, el nombre
  de esta carpeta será el nombre del alumno, sin espacios, escribiendo con
  mayúscula la primer letra de cada nombre y apellido 
  (ej. `tarea-01/EmilioCabrera`)
- Para las entregas en equipo se creará una carpeta por equipo, el nombre de la
  carpeta será formada por las inciales de cada integrante (ej. `practica-00/equipo-JLT-ET-EC`)
- Cada tarea o práctica deberá tener lo siguiente:
    - Un archivo `README.md` donde se entregará la documentación en formato _markdown_.
      - Si la entrega es en equipo se debe incluir el nombre completo de los 
        integrantes
      - Se puede hacer uso de un directorio `img/` para guardar las imágenes o 
        capturas necesarias para la documentación.
    - Una carpeta `src` donde se encontrará el codigo de la tarea o práctica
    - Un archivo `Makefile` que servirá para compilar el programa y hacer pruebas

```
tareas-so/
├── img/
│   ├── 000-workflow.png
│   └── ...
├── README.md
├── tarea-01/
│   ├── EmilioCabrera/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── src/
│   │   │   └── ...
│   │   ├── Makefile
│   │   └── README.md
│   └── ...
├── tarea-02/
│   ├── equipo-JLT-ET-EC/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── src/
│   │   │   └── ...
│   │   ├── Makefile
│   │   └── README.md
│   └── ...
├── practica-00/
│   ├── EmilioCabrera/
│   │   └── README.md
│   └── ...
├── practica-01/
│   ├── equipo-JLT-ET-EC/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── src/
│   │   │   └── ...
│   │   ├── Makefile
│   │   └── README.md
│   └── ...
└── practica-mm/
    └── ...
```

--------------------------------------------------------------------------------

## Flujo de trabajo para la entrega de tareas

![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")

### Acceder al repositorio principal

- Abre la URL del [repositorio de tareas para la materia][repositorio-tareas]

![](img/001-Main_repo.png "")

- [Inicia sesión en GitLab][gitlab-login]

![](img/002-Sign_in.png "")

--------------------------------------------------------------------------------

### Hacer `fork` al repositorio principal

- Da clic en el botón `fork` para crear una copia del repositorio `tareas-so` en tu cuenta de usuario

![](img/003-Fork_repo.png "")

- Espera a que el __fork__ se complete

![](img/004-Fork_in_progress.png "")

--------------------------------------------------------------------------------

### Clonar el repositorio

- Accede a la URL del repositorio `tareas-so` asociado a **tu cuenta de usuario**

```
https://gitlab.com/USUARIO/tareas-so.git
```

- Obten la URL de tu repositorio `tareas-so` y bájalo a tu equipo con `git clone`:

![](img/005-Fork_successful-clone_URL.png "")

```
$ git clone https://gitlab.com/USUARIO/tareas-so.git
Cloning into 'tareas-so'...
remote: Enumerating objects: 24, done.
remote: Total 24 (delta 0), reused 0 (delta 0), pack-reused 24
Receiving objects: 100% (24/24), 1.47 MiB | 848.00 KiB/s, done.
Resolving deltas: 100% (1/1), done.
```

- Lista el contenido

```
$ cd tareas-so/
$ ls -lA
total 28
drwxrwxr-x 8 gecko gecko  4096 mar  8 20:59 .git
-rw-rw-r-- 1 gecko gecko  4493 mar  8 20:59 .gitignore
drwxrwxr-x 2 gecko gecko  4096 mar  8 20:59 img
-rw-rw-r-- 1 gecko gecko 11018 mar  8 20:59 README.md
```

--------------------------------------------------------------------------------

### Crear rama personal

- Crea una rama con tu nombre para versionar tus cambios

```
$ git checkout -b EmilioCabrera
Switched to a new branch 'EmilioCabrera'
```

- Comprueba que te encuentres en la rama con tu nombre. Debe tener el prefijo `*`

```
$ git branch
* EmilioCabrera
  master
```

--------------------------------------------------------------------------------

### Agregar carpeta personal

- Accede al repositorio y crea una carpeta para la entrega con tu nombre si es
  una entrega individual o el nombre de tu equipo.

```
$ $ mkdir -vp practica-00/EmilioCabrera
mkdir: created directory 'practica-00'
mkdir: created directory 'practica-00/EmilioCabrera'
```

>>>
Dentro de esa carpeta es donde debes poner los archivos de la entrega
>>>

--------------------------------------------------------------------------------

### Agregar archivo con tu nombre

- Crea un archivo de _markdown_ llamado `README.md` que contenga tu nombre o el
  nombre de **todos** los integrantes tu equipo dependiendo del tipo de entrega.

>>>
Observa el _contenido de ejemplo_ en el siguiente paso
>>>

```
$ editor practica-00/EmilioCabrera/README.md
```

- Contenido de ejemplo para el archivo README.md 

```
NOMBRE COMPLETO
NÚMERO DE CUENTA

Esta es la carpeta de NOMBRE COMPLETO
```

--------------------------------------------------------------------------------

### Enviar cambios al repositorio

- Una vez que hayas creado los archivos, revisa el estado del repositorio

```
$ git status
On branch EmilioCabrera
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        practica-00/

nothing added to commit but untracked files present (use "git add" to track)
```

- Lista el directorio y agrega los archivos con `git add`

```
$ cd practica-00/EmilioCabrera/
$ ls -lA
total 4
-rw-rw-r-- 1 gecko gecko 62 mar  8 21:54 README.md

$ git add README.md
```

- Versiona los archivos con `git commit`

>>>
Usa **comillas simples** para especificar el mensaje del _commit_
>>>

```
$ git commit -m 'Carpeta de Emilio Cabrera'
[EmilioCabrera c9844a6] Carpeta de Emilio Cabrera
 1 file changed, 4 insertions(+)
 create mode 100644 practica-00/EmilioCabrera/README.md
```

+ Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```
$ git remote -v
origin  git@gitlab.com:emilio1625/tareas-so.git (fetch)
origin  git@gitlab.com:emilio1625/tareas-so.git (push)
```

+ Revisa la rama en la que estas para enviarla a GitLab

```
$ git branch
* AndresHernandez
  entregas
```

+ Envía los cambios a **tu repositorio** utilizando `git push`

```
$ git push -u origin AndresHernandez
Username for 'https://gitlab.com': USUARIO
Password for 'https://USUARIO@gitlab.com':
Counting objects: 5, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (5/5), 414 bytes | 138.00 KiB/s, done.
Total 5 (delta 1), reused 0 (delta 0)
remote: 
remote: To create a merge request for EmilioCabrera, visit:
remote:   https://gitlab.com/emilio1625/tareas-so/-/merge_requests/new?merge_request%5Bsource_branch%5D=EmilioCabrera
remote: 
To gitlab.com:emilio1625/tareas-so.git
 * [new branch]      EmilioCabrera -> EmilioCabrera
Branch 'EmilioCabrera' set up to track remote branch 'EmilioCabrera' from 'origin'.
```

--------------------------------------------------------------------------------

### Crea un `merge request` para entregar tu tarea

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

+ Accede a la url de **tu repositorio**:

```
https://gitlab.com/USUARIO/tareas-so
```

![](img/006-Fork-commit_ok.png "")

+ Selecciona la rama con tu nombre

![](img/007-Fork_select_branch.png "")

+ Verifica que aparezca el título de tu commit

![](img/008-Fork_new_branch.png "")

+ Ve a la sección llamada `merge requests` en la barra lateral

+ Crea un nuevo `merge request` para enviar los cambios al repositorio central

![](img/009-Fork-new_MR.png "")

--------------------------------------------------------------------------------

#### Llena los datos del _merge request_

+ Selecciona _la rama con tu nombre_ como **origen** y la rama `tareas-so` como **destino**

![](img/010-Fork-MR_data-branch.png "")

+ Escribe un título y una descripción que sirva como vista previa para tu entrega

+ Da clic en el botón _submit_ para crear tu _merge request_

![](img/011-Fork-MR_data.png "")

--------------------------------------------------------------------------------

#### Notificaciones de creación y seguimiento del MR

+ Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios

![](img/012-Main_MR_created.png "")

+ Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

![](img/013-Main_MR_merged.png "")

--------------------------------------------------------------------------------

>>>
**Protip**
Puedes cambiar la visibilidad del proyecto a privada

+ En la barra lateral seleccionar `Settings` > `General`
+ Expande la parte llamada `Permissions`, ahí puedes seleccionar la visibilidad deseada
+ Da clic en `Save changes` para aplicar
>>>

--------------------------------------------------------------------------------

### Vista en GitLab

+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-2/tareas-so>

--------------------------------------------------------------------------------

[url-sistemas_operativos-fciencias]: http://www.fciencias.unam.mx/docencia/horarios/presentacion/322385 "Sistemas Operativos 2021-2"

[repositorio-tareas]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-2/tareas-so
[gitlab-login]: https://gitlab.com/users/sign_in
