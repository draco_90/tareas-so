#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#define LONG_CHAR 32
#define SIZE_ARRAY 5
#define SUCCESS 0
#define BUFFER_SIZE 32
#define DEV_NAME "DMFAHUYM"

static int opened_devices = 0;

static unsigned char entero = 0;
module_param(entero, byte, 0);
MODULE_PARM_DESC(entero, "Entero sin signo de 8 bits. Prioridad más alta");


static char *cadena[LONG_CHAR];
static int tamanio;
module_param_array(cadena, charp, &tamanio, 0);
MODULE_PARM_DESC(cadena, "Cadena. Segunda máxima prioridad");

static int arr_size = SIZE_ARRAY;
static int arreglo[SIZE_ARRAY];
module_param_array(arreglo, int, &arr_size, 0);
MODULE_PARM_DESC(arreglo, "Arreglo. Tercera prioridad");

static int cont_dif_ceros;
static int i;


/* La función device_open informa al kernel que el módulo esta en uso y
 * no puede ser eliminado usando `rmmod`
 */
static int device_open(struct inode *i, struct file *f)
{
    if (opened_devices)
    {
        return -EBUSY;
    }
    opened_devices++;
    printk(KERN_INFO DEV_NAME ": Dispositivo abierto");
    try_module_get(THIS_MODULE);
    return SUCCESS;
}

/* La función device_read copia al *buffer* el parametro de mayor prioridad */
static ssize_t device_read(struct file *flip, char *buffer, size_t length, loff_t * offset)
{
    int reads = 0;
    if (entero != 0)
    {
        int size = sizeof(entero);
        copy_to_user(buffer, entero, BUFFER_SIZE);
        entero = 0;
        buffer++;
        return size;
    }
    else if(*cadena != 0)
    {
        while(length && *cadena)
        {
            copy_to_user(buffer, cadena, BUFFER_SIZE);
            length -= strlen(cadena);
            reads += strlen(cadena);
        }
    }
    return reads;
    return 0;
}

/* La función device_release informa al kernel que ya no esta siendo 
 * usado.
 */
static int device_release(struct inode *i, struct file *f)
{
    opened_devices--;
    printk(KERN_INFO DEV_NAME ": Dispositivo cerrado");
    module_put(THIS_MODULE);
    return 0;
}

/* Devuelve error al kernel ya que no se encuentra implementado */
static ssize_t device_write(struct file *f, const char *c, size_t s, loff_t *o)
{
    printk(KERN_ALERT "Operación no implementada " DEV_NAME "\n");
    return -EINVAL;
}
