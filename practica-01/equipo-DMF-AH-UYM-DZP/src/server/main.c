#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/wait.h>
//Función encargada de manejar errores
#define handle_error(msg)   \
    do                      \
    {                       \
        perror(msg);        \
        exit(EXIT_FAILURE); \
    } while (0)
#define MAX_PROCESSES 4 //número de procesos máximos
char indexHTML[] =
    "HTTP/1.1 200 OK\r\n"
    "Content-Type: text/html; charset=UTF-8\r\n"
    "<!!DOCTYPE html>\r\n"
    "<html><head><title>WebServer equipo-DMF-AH-UYM-DZP</title></head>\r\n"
    "<body>Hola Mundo</body></html>\r\n";
//Función que obtiene el archivo solicitado completo, es decir sin necesidad de traerlo por partes
char *getfile(char *uri)
{
    FILE *f = fopen(uri, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* same as rewind(f); */

    char *string = malloc(fsize + 1);
    fread(string, 1, fsize, f);
    fclose(f);
    string[fsize] = '\0';
    return string;
}
//malloc or die
void *xmalloc(size_t size)
{
    void *m = malloc(size);
    if (m == NULL)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    return m;
}
//Función encargada de contestar al cliente cuando hay una consulta mal construída
void badrequest(FILE *cfp)
{
    fputs("HTTP/1.1 400 Bad Request\r\n", cfp);
    fseek(cfp, 0, SEEK_END);
}
//Función encargada de contestar al cliente cuando se pide un método que no se tiene contemplado
void notimplemented(FILE *cfp)
{
    fputs("HTTP/1.1 501 Not implemented\r\n", cfp);
    fseek(cfp, 0, SEEK_END);
}
//Función encargada de contestar al cliente que no tiene permiso de ver el archivo que quiere obtener
void forbidden(FILE *cfp)
{
    fputs("HTTP/1.1 403 Forbidden\r\n", cfp);
    fseek(cfp, 0, SEEK_END);
}
//Función encargada de contestar al cliente que no se encontró el archivo que buscaba
void notfoud(FILE *cfp)
{
    fputs("HTTP/1.1 404 Not Found\r\n", cfp);
    fseek(cfp, 0, SEEK_END);
}
//Función que devuelve el directorio donde se está ejecutando el server
char *currentworkingdirectory()
{
    char cwd[1256];
    if (getcwd(cwd, sizeof(cwd)) == NULL)
        perror("getcwd() error");
    return strdup(cwd);
}
//Función que valida que el archivo exista y que el cliente tenga permiso para verlo
int validatefile(char *uri, FILE *cfp)
{
    fprintf(stderr, "Uri %s\n", uri);
    int fileexists = access(uri, F_OK);
    if (fileexists != 0)
    {
        fprintf(stderr, "Validate Existt %s\n", uri);
        notfoud(cfp);
        return 1;
    }
    else
    {
        fprintf(stderr, "Validate Uri %s\n", uri);
        char *currentfolder = currentworkingdirectory();
        if (strstr(uri, currentfolder) == NULL)
        {
            forbidden(cfp);
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
//Función principal del server
int main(int argc, char const *argv[])
{

    struct sockaddr_in caddr = {0};

    /* code */
    //abrir un socket
    int sfd = socket(PF_INET, SOCK_STREAM, 0);
    if (sfd == -1)
        handle_error("socket");

    /* Configuramos las opciones del socket.
     * Pasamos nuestro descriptor sfd
     * Pasamos SOL_SOCKET, primera capa del socket usada para opciones independientes del socket
     * Pasamos SO_REUSEADDR, que especifica las reglas de validación de las direcciones suministradas a bind()
     * Pasamos optval = 1
     * Pasamos el tamaño del socket
     */
    {
        int optval = 1;
        if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == -1)
            handle_error("setsockopt");
    }

    //asignar dirección y puesto
    struct sockaddr_in saddr = {
        .sin_family = AF_INET,         /* familia de direcciones: AF_INET */
        .sin_port = htons(8080),       /* puerto con los bytes en el orden de red */
        .sin_addr.s_addr = INADDR_ANY, /* dirección de Internet */
    };

    //bind y listen

    if (bind(sfd, (struct sockaddr *)&saddr, (socklen_t)sizeof(saddr)) == -1)
        handle_error("bind");

    if (listen(sfd, 4) == -1)
        handle_error("listen");

    int cfd = -1;
    signal(SIGCHLD, SIG_IGN);
    int procesos = 0;

    while (1)
    {

        fprintf(stderr, "Procesos Max %d\n", MAX_PROCESSES);
        fprintf(stderr, "Procesos %d\n", procesos);
        //Configuramos el socket
        socklen_t len = sizeof(caddr);
        cfd = accept(sfd, (struct sockaddr *)&caddr, &len);
        if (cfd == -1)
            handle_error("accept");

        openlog("DMF-AH-UYM-DZP WebServer", LOG_PID, LOG_USER);
        syslog(LOG_INFO, "Start logging");
        syslog(
            LOG_INFO,
            "Nuevo cliente en el puerto %d con dirección ip %s\n",
            ntohs(caddr.sin_port),
            inet_ntoa(caddr.sin_addr));
        syslog(LOG_INFO, "End logging");
        closelog();

        int childProcess = fork();

        if (childProcess == -1)
            handle_error("A client was not accepted");
        if (childProcess == 0)
        {
            close(sfd);
            FILE *cfp = fdopen(cfd, "a+");
            char *method, *uri, *version;
#define BUF_SIZE 4096
            char *buff = xmalloc(BUF_SIZE);
            int requests = 0;
            while (requests <= 5)
            {
                printf("Requests %d\n", requests);
                fgets(buff, BUF_SIZE, cfp);
                sscanf(buff, "%ms %ms %ms\r\n", &method, &uri, &version);
                if (strcmp(version, "HTTP/1.1") != 0) //Validamos que la consulta esté bien formada
                {
                    badrequest(cfp);
                }
                if (strcmp("GET", method) == 0) //Validamos que la consulta esté bien formada
                {

                    char *param = strchr(uri, '?'); //Eliminamos parámetros extra de la consulta
                    if (param != NULL)
                        param = '\0';
                    if (strcmp(uri, "/") == 0) //En caso de que se envíe una carpeta y no un archivo devolvemos el archivo index.html
                        fputs(indexHTML, cfp);
                    if (validatefile(uri, cfp) == 0)
                    {
                        char *file = strcat(getfile(uri), "\r\n");       //Obtenemos el contenido del archivo
                        fputs(strcat("HTTP/1.1 200 OK\r\n", file), cfp); //Enviamos el contenido del archivo
                    }
                }
                else
                {
                    badrequest(cfp);
                }
                //Limpiamos la memoria
                fflush(NULL);
                free(method);
                free(uri);
                free(version);
                requests++;
            }
            //Cerramos el proceso
            shutdown(cfd, 2);
            close(cfd);
            exit(0);
        }
        else
        //Aumentamos la cuenta de los procesos activos
        {
            procesos++;
        }
    }
    return 0;
}
